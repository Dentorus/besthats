<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}LF7B0$[q!dW5QJ(3*zs4SmET,F>ys?_D zWd9+Vk7KNc* @h;C71 t4P$ww-x9:');
define('SECURE_AUTH_KEY',  ',J-2Yl3rV-94A/xk]Q}PkN?1R!]-RPF7KBiZkd@eyH-v0^Q K$SQsFl<ya9iz`Bc');
define('LOGGED_IN_KEY',    'y%Q$<ja9KAz@[oZjA)^;LDq3I07XU$cJ/DMtx[;bcG[Zl`IHWc[K=k2{w4zmwjk{');
define('NONCE_KEY',        'Fe-y]17q/@~#n{s~G-<10VN[;%y&U5P#Tya6{~-l-jt-&lN;ZPaF#69<gWaL%E6Y');
define('AUTH_SALT',        '1h@Hk*-_k07>*=r4y),E6x$ Dw1;]|))G{:w}A@JR8DlkoJOcEmi1AC{o<6<o-X&');
define('SECURE_AUTH_SALT', 'OTLiH6?1ml mmzA8W^,`Ty9)58;a)GWAg3Rw@Y9zJ8=9^(7&xchBC2mJi8raM<~G');
define('LOGGED_IN_SALT',   'H-k^^>6H}+%QO_=@l,$mCa2)lB&-U;N1^Y^ $?DvnsLi_]l&FW|5h!TmPki{[h?3');
define('NONCE_SALT',       'G]0Ndxm;=EX*~gPuLLt[W|7,TB`dk`3J}TT*=~KvZ]%X6k%J)<`zQ_8T58BjXlx9');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
