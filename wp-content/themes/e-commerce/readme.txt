=== E-Commerce ===

Contributors: Catch Themes and Emil Uzelac 
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments
Requires at least: 4.2
Tested up to: 4.5
Stable tag: 1.1
E-commerce WordPress Theme, Copyright 2012-2016 Catchthemes.com
E-commerce is distributed under the terms of the GNU General Public License v3 


== Description ==

E-Commerce is a clean responsive e-commerce WordPress theme designed especially to work with a popular WooCommerce plugin. It was designed from the start to look awesome on any device. Theme provides a minimalist but modern look.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

includes support for Infinite Scroll and Responsive Videos in Jetpack.

== Change Log ==

Please refer to changelog.txt file or change log page at https://catchthemes.com/changelogs/e-commerce-theme/

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* Sidr https://github.com/artberri/sidr, (C) 2013 Alberto Varela, [MIT](http://opensource.org/licenses/MIT)
